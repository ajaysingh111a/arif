﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="arif.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 44px;
        }
    </style>
</head>
<body>
    <center/>
    <form id="form1" runat="server">
    <div>
        <table border="0">
            <tr style="color:red;font-size:20px;">
                <th colspan="2" class="auto-style1">Software Firm</th>
                </tr>
            <tr>
                <td>First Name:</td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter first Name in String" ControlToValidate="TextBox1" type="String"></asp:RequiredFieldValidator>

                </td>
            </tr>

            <tr>
                <td>Last Name:</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter last name in String" ControlToValidate="TextBox2" Type="String"></asp:RequiredFieldValidator></td>
                </tr>

             <tr>
                    <td>
                        Password
                    </td>
                    <td>
                        <asp:TextBox ID="txtpwd" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Confirm Password
                    </td>
                    <td>
                        <asp:TextBox ID="txtcpwd" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:CompareValidator ID="cvPwd" runat="server" ErrorMessage="Password doesn't match!"
                            ControlToValidate="txtcpwd" ControlToCompare="txtpwd"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                <td>Age</td>
                <td>
                <asp:TextBox  ID="txtage" runat="server" MaxLength=2></asp:TextBox>
                <asp:CompareValidator  ID="cvcage" runat="server" ErrorMessage="Please Enter a number" ControlToValidate="txtage" Type="Integer" Operator="DataTypeCheck"></asp:CompareValidator></td>
                </tr>

             <tr>
                    <td>
                        City:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddCity" runat="server">
                            <asp:ListItem Value="Mumbai" Selected="True">Mumbai</asp:ListItem>
                            <asp:ListItem Value="Delhi">Delhi</asp:ListItem>
                            <asp:ListItem Value="Banglore">Banglore</asp:ListItem>
                            <asp:ListItem Value="Kolkata">Kolkata</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>

             <tr>
                    <td>
                        Gender:
                    </td>
                    <td>
                        <asp:RadioButton ID="rdoMale" runat="server" Text="Male" GroupName="Gender" />
                        </br>
                        <asp:RadioButton ID="rdoFemale" runat="server" Text="Female" GroupName="Gender" />
                    </td>
                </tr>

             <tr>
                    <td>
                        Do you like Music?:
                    </td>
                    <td>
                        <asp:CheckBox ID="chkMusic" runat="server" Text="Yes" />
                    </td>
                </tr>



        </table>

        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click1"></asp:Button>
        <input type="reset"/>
       
    </div>
    </form>
        </center>
</body>
</html>
